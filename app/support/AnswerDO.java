package support;

import cn.hutool.json.JSONUtil;

public class AnswerDO {
    public String answer;

    public boolean isAnswer;

    public String toJson(){
        return JSONUtil.toJsonStr(this);
    }
}
