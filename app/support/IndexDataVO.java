package support;

import models.VideoIndex;

import java.util.List;

public class IndexDataVO {

    public IndexDataVO(List<AppBanner> list) {
        this.banners = new Banner();
        this.banners.list = list;
    }

    public Banner banners;

    public List<VideoIndex> videos;


    public class Banner {
        public List<AppBanner> list;
    }
}
