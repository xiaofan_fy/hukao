package support.exception;

public class ParamsValidateException extends RuntimeException {
    public ParamsValidateException(String msg) {
        super(msg);
    }
}
