package support;

import cn.hutool.json.JSONUtil;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.tom.basic.serialize.json.GsonField;
import models.base.BaseModel;
import org.apache.commons.lang.StringUtils;

import javax.persistence.Entity;
import java.util.List;

@Entity
public class BigCate extends BaseModel {

    public String name;

    public Integer sortOrder;

    public String bannerUrls;

    @GsonField(name = "bannerUrlsShow")
    public List<AppBanner> bannerUrlsShow() {
        if (StringUtils.isEmpty(bannerUrls)) {
            return Lists.newArrayList();
        }
        return JSONUtil.toList(bannerUrls, AppBanner.class);
    }

}
