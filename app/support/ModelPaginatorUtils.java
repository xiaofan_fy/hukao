package support;

import com.tom.basic.service.BaseQuery;

import javax.persistence.Query;
import java.util.List;

public class ModelPaginatorUtils {

	public static void setParams2Query(List<Object> params, Query query) {
		for (int i = 0; i < params.size(); i++) {
			query.setParameter(i + 1, params.get(i));
		}
	}

	public static void setParams2Query(List<Object> params, Query query,
			BaseQuery baseQuery) {
		for (int i = 0; i < params.size(); i++) {
			query.setParameter(i + 1, params.get(i));
		}
		query.setFirstResult(baseQuery.getStartPos());
		query.setMaxResults(baseQuery.getPageSize());
	}

}
