package support;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogUtils {

    public static final Logger ERROR_LOGGER = LoggerFactory.getLogger("SYSTEMERROR");

    public static final Logger ACCESS_LOGGER = LoggerFactory.getLogger("ACCESS");

}
