package utils;

import play.Play;

public class BizConstants {

    /**
     * 当前是否属于生产环境
     *
     * @return
     */
    public static String APPLICATION_RUN_MODE = Play.configuration.getProperty("application.mode");


    public static boolean isInProduct() {
        return "prod".equalsIgnoreCase(APPLICATION_RUN_MODE);
    }


    public static String TIME_SECOND_FORMATE = "yyyy-MM-dd HH:mm:ss";


}
