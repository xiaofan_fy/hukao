package utils;

public class MenuData {
    public static String MENU_DATA = "{\"code\":0,\"data\":[{\"path\":\"/home\",\"component\":\"home\",\"meta\":{\"isLink\":\"\"," +
            "\"isIframe\":false,\"roles\":[\"admin\",\"common\"],\"icon\":\"iconfont icon-shouye\",\"isAffix\":true,\"title\":\"message" +
            ".router.home\",\"isHide\":false,\"isKeepAlive\":true},\"name\":\"home\"},{\"path\":\"/userAccount\"," +
            "\"component\":\"userAccount\",\"meta\":{\"isLink\":\"\",\"isIframe\":false,\"roles\":[\"admin\",\"common\"]," +
            "\"icon\":\"iconfont icon-gongju\",\"isAffix\":false,\"title\":\"message.router.userAccount\",\"isHide\":false," +
            "\"isKeepAlive\":true},\"name\":\"userAccount\"},{\"redirect\":\"/menu/menu1\",\"path\":\"/menu\"," +
            "\"component\":\"layout/routerView/parent\",\"children\":[{\"path\":\"/bigcate\",\"component\":\"bigcate\"," +
            "\"meta\":{\"isLink\":\"\",\"isIframe\":false,\"roles\":[\"admin\",\"common\"],\"icon\":\"iconfont icon-caidan\"," +
            "\"isAffix\":false,\"title\":\"message.router.menu2\",\"isHide\":false,\"isKeepAlive\":true},\"name\":\"menu2\"}," +
            "{\"path\":\"/tiku\",\"component\":\"tiku\",\"meta\":{\"isLink\":\"\",\"isIframe\":false,\"roles\":[\"admin\",\"common\"]," +
            "\"icon\":\"iconfont icon-caidan\",\"isAffix\":false,\"title\":\"message.router.tiku\",\"isHide\":false," +
            "\"isKeepAlive\":true},\"name\":\"tiku\"},{\"path\":\"/video\",\"component\":\"video\",\"meta\":{\"isLink\":\"\"," +
            "\"isIframe\":false,\"roles\":[\"admin\",\"common\"],\"icon\":\"iconfont icon-caidan\",\"isAffix\":false,\"title\":\"message" +
            ".router.video\",\"isHide\":false,\"isKeepAlive\":true},\"name\":\"video\"},{\"path\":\"/kejian\",\"component\":\"kejian\"," +
            "\"meta\":{\"isLink\":\"\",\"isIframe\":false,\"roles\":[\"admin\",\"common\"],\"icon\":\"iconfont icon-caidan\"," +
            "\"isAffix\":false,\"title\":\"message.router.kejian\",\"isHide\":false,\"isKeepAlive\":true},\"name\":\"kejian\"}," +
            "{\"path\":\"/kejianCate\",\"component\":\"kejianCate\",\"meta\":{\"isLink\":\"\",\"isIframe\":false,\"roles\":[\"admin\"," +
            "\"common\"],\"icon\":\"iconfont icon-caidan\",\"isAffix\":false,\"title\":\"message.router.kejianCate\",\"isHide\":false," +
            "\"isKeepAlive\":true},\"name\":\"kejianCate\"}],\"meta\":{\"isLink\":\"\",\"isIframe\":false,\"roles\":[\"admin\",\"common\"]," +
            "\"icon\":\"iconfont icon-caidan\",\"isAffix\":false,\"title\":\"message.router.menu\",\"isHide\":false," +
            "\"isKeepAlive\":true},\"name\":\"menu\"}]}";


    public static void main(String[] args) {
        System.out.println(MENU_DATA);
    }
}
