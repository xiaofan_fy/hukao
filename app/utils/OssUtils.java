package utils;

import com.aliyun.oss.ClientConfiguration;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.PutObjectResult;
import com.tom.basic.service.ServiceResult;
import support.LogUtils;

import java.io.ByteArrayInputStream;
import java.io.File;

public class OssUtils {

    public static Object LOCK = new Object();

    private OSSClient ossClient;

    private static final String ENDPOINT = "oss-cn-shanghai.aliyuncs.com";

    private static final String ACCESS_KEY = "LTAI5tSmMDm3Ntom2o9J2E78";

    private static final String ACCESS_SECRET = "m2sXELM2tKyC8qI0P4XfeFbmvDb6rw";

    private static final String GLOBAL_CF_OSS_CDN_HOST = "junlekeji.devtools.cool/";

    private static final String BUCKET_NAME = "junlekeji";

    private static final String FOLDER_NAME = "junlekeji/";

    private static OssUtils ossUtils;

    private OssUtils() {
    }

    public static OssUtils getClient() {
        if (ossUtils != null) {
            return ossUtils;
        }
        synchronized (LOCK) {
            ossUtils = new OssUtils();
            //初始化
            ClientConfiguration conf = new ClientConfiguration();
            conf.setSupportCname(true);
            ossUtils.ossClient = new OSSClient(ENDPOINT, ACCESS_KEY, ACCESS_SECRET, conf);
        }
        return ossUtils;
    }


    /**
     * 上传文件
     *
     * @param key
     * @param file
     */
    private boolean uploadFile(String key, File file) {
        try {
            PutObjectResult res = ossUtils.ossClient.putObject(BUCKET_NAME, buildKey(key), file);
            return true;
        } catch (Exception e) {
            LogUtils.ERROR_LOGGER.error("上传静态资源报错", e);
            return false;
        }
    }

    /**
     * 上传byte数组
     *
     * @param key
     * @param data
     */
    public ServiceResult<String> upload(String key, byte[] data) {
        try {
            ossUtils.ossClient.putObject(BUCKET_NAME, buildKey(key), new ByteArrayInputStream(data));
            return ServiceResult.asSuccess(getFullDownloadPath(key));
        } catch (Exception e) {
            LogUtils.ERROR_LOGGER.error("uploade file to aliyun failed", e);
            return ServiceResult.asFail(e.getMessage());
        }
    }

    private String buildKey(String fileName) {
        return FOLDER_NAME + fileName;
    }

    /**
     * 上传文件
     *
     * @param key
     * @param file
     */
    public ServiceResult<String> upload(String key, File file) {
        boolean uploadResult = uploadFile(key, file);
        if (!uploadResult) {
            return ServiceResult.asFail("upload fail, please try.");
        }

        return ServiceResult.asSuccess(getFullDownloadPath(key));
    }

    public String getFullDownloadPath(String key) {
        return "https://" + GLOBAL_CF_OSS_CDN_HOST + buildKey(key);
    }


    public static void main(String[] args) {
        ServiceResult<String> oss = OssUtils.getClient().upload(System.nanoTime() + "", "text".getBytes());
        System.out.println(oss.obj);
    }

}
