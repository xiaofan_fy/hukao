package utils;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

public class OrderMetaUtils {


    public Long getMetaLongVal(String orderMeta, String key) {
        String valueStr = getMetaValue(orderMeta, key);
        if (StringUtils.isEmpty(valueStr)) {
            return null;
        }
        return Long.parseLong(valueStr);
    }
    
    public static String addMeta(String orderMeta, String key, String value) {
        if (StringUtils.isNotBlank(key) && StringUtils.isNotBlank(value)) {
            Map<String, String> orderMetaMap = Maps.newHashMap();
            if (StringUtils.isNotBlank(orderMeta)) {
                String[] mataKVS = orderMeta.split(";");
                for (String eachKV : mataKVS) {
                    String[] kvArr = eachKV.split(":");
                    orderMetaMap.put(kvArr[0], kvArr[1]);
                }
            }
            orderMetaMap.put(key, value);

            orderMeta = "";
            int index = 0;
            for (String innerKey : orderMetaMap.keySet()) {
                if (index == 0) {
                    orderMeta = innerKey + ":" + orderMetaMap.get(innerKey);
                } else {
                    orderMeta = orderMeta + ";" + innerKey + ":" + orderMetaMap.get(innerKey);
                }
                index++;
            }

        }
        return orderMeta;
    }

    public static String removeMeta(String orderMeta, String key) {
        if (StringUtils.isNotBlank(key) && StringUtils.isNotBlank(orderMeta)) {
            Map<String, String> orderMetaMap = Maps.newHashMap();
            String[] mataKVS = orderMeta.split(";");
            for (String eachKV : mataKVS) {
                String[] kvArr = eachKV.split(":");
                if (!key.equals(kvArr[0])) {
                    orderMetaMap.put(kvArr[0], kvArr[1]);
                }
            }

            orderMeta = "";
            int index = 0;
            for (String innerKey : orderMetaMap.keySet()) {
                if (index == 0) {
                    orderMeta = innerKey + ":" + orderMetaMap.get(innerKey);
                } else {
                    orderMeta = orderMeta + ";" + innerKey + ":" + orderMetaMap.get(innerKey);
                }
                index++;
            }
        }
        return orderMeta;
    }

    public static String getMetaValue(String orderMeta, String key) {
        Map<String, String> orderMetaMap = getOrderMetaMap(orderMeta);
        if (orderMetaMap.isEmpty()) {
            return null;
        } else {
            return orderMetaMap.get(key);
        }
    }

    public static Map<String, String> getOrderMetaMap(String orderMeta) {
        Map<String, String> orderMetaMap = Maps.newHashMap();
        if (orderMetaMap.isEmpty()) {
            orderMetaMap = Maps.newHashMap();
            if (StringUtils.isNotBlank(orderMeta)) {
                String[] mataKVS = orderMeta.split(";");
                for (String eachKV : mataKVS) {
                    String[] kvArr = eachKV.split(":");
                    orderMetaMap.put(kvArr[0], kvArr[1]);
                }
            }
        }
        return orderMetaMap;
    }

}
