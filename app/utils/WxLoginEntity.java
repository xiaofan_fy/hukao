package utils;

import java.io.Serializable;

public class WxLoginEntity implements Serializable {

    public String code;

    public String encryptedData;

    public String signature;

    public String iv;

    public UserInfo userInfo;
}
