package utils;

public class UserInfo {

    public String avatarUrl;
    public String city;
    public int gender;
    public String nickName;
    public String province;
}
