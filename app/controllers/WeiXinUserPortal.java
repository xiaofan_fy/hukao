package controllers;

import com.tom.basic.service.ServiceQueryResult;
import controllers.filter.FilterAdminPortal;
import models.services.WeiXinUserService;
import play.mvc.Controller;
import play.mvc.With;
import support.BigCate;

@With(FilterAdminPortal.class)
public class WeiXinUserPortal extends Controller {

    public static void queryWeiXinUser(int page, int limit) {
        renderJSON(WeiXinUserService.queryUsers(page, limit).toJson());
    }

    // 获取大类
    public static void bigCate() {
        renderJSON(ServiceQueryResult.asSuccess(BigCate.find("isDelete = 0").fetch()).toJson());
    }
}
