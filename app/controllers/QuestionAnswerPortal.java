package controllers;

import cn.hutool.json.JSONUtil;
import com.tom.basic.service.ServiceQueryResult;
import com.tom.basic.service.ServiceResult;
import controllers.filter.FilterAdminPortal;
import models.ChildSubjectDictionary;
import models.QuestionAnswer;
import models.SubjectDictionary;
import models.services.QuestionAnswerService;
import play.mvc.Controller;
import play.mvc.With;
import support.BigCate;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@With(FilterAdminPortal.class)
public class QuestionAnswerPortal extends Controller {

    public static void addQa(Long id, Long bigCateId, Long subCateId, Long childCateId, String question, String answerDesc,
                             List<String> answers, String videoUrl, Integer sortOrder) {
        QuestionAnswer questionAnswer = new QuestionAnswer();
        if (id != null) {
            questionAnswer = QuestionAnswer.findById(id);
        }
        questionAnswer.bigCateId = bigCateId;
        questionAnswer.subCateId = subCateId;
        questionAnswer.childCateId = childCateId;
        questionAnswer.question = question;
        questionAnswer.selectAbleData = JSONUtil.toJsonStr(answers);
        questionAnswer.answerDesc = answerDesc;
        questionAnswer.videoUrl = videoUrl;
        questionAnswer.sortOrder = sortOrder;
        questionAnswer.save();
        renderJSON(ServiceResult.asSuccess("操作成功"));
    }


    public static void delQa(Long id) {
        QuestionAnswer questionAnswer = QuestionAnswer.findById(id);
        if (questionAnswer != null) {
            questionAnswer.isDelete = 1;
            questionAnswer.gmtModify = new Date();
            questionAnswer.save();
        }
        renderJSON(ServiceResult.asSuccess("删除成功"));
    }

    public static void queryQA(Long id, Long bigCateId, Long subCateId, Long childCateId, int page, int limit) {
        List<BigCate> bigCates = BigCate.findAll();
        Map<Long, BigCate> bigCateMap = bigCates.stream().collect(Collectors.toMap(e -> e.id, e -> e));
        List<SubjectDictionary> subjectDictionaries = SubjectDictionary.findAll();
        Map<Long, SubjectDictionary> subjectDictMap = subjectDictionaries.stream().collect(Collectors.toMap(e -> e.id, e -> e));
        List<ChildSubjectDictionary> childSubjectDictionaries = ChildSubjectDictionary.findAll();
        Map<Long, ChildSubjectDictionary> childSubjectDictMap = childSubjectDictionaries.stream().collect(Collectors.toMap(e -> e.id,
                e -> e));
        ServiceQueryResult<QuestionAnswer> resData = QuestionAnswerService.queryData(id, bigCateId, subCateId, childCateId, page, limit);
        resData.obj.forEach(e -> {
            BigCate bigCate = bigCateMap.get(e.bigCateId);
            SubjectDictionary subjectDictionary = subjectDictMap.get(e.subCateId);
            ChildSubjectDictionary childSubjectDictionary = childSubjectDictMap.get(e.childCateId);
            e.bigCateShow = bigCate.name;
            e.subCateShow = subjectDictionary.dictName;
            e.childCateShow = childSubjectDictionary.dictName;
            e.vipSubShow = subjectDictionary.vipSubShow();
        });
        renderJSON(resData.toJson());
    }
}
