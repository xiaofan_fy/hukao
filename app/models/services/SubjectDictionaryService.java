package models.services;

import com.google.common.collect.Maps;
import models.QuestionAnswer;
import models.SubjectDictionary;

import java.util.List;
import java.util.Map;

public class SubjectDictionaryService {

    public static List<SubjectDictionary> querySubDict(Long bigCate) {
        return SubjectDictionary.find("isDelete = 0 and bigCateId=:bigCateId order by sortOrder desc").setParameter("bigCateId", bigCate).fetch();
    }

    /**
     * 查询大类下，各个章节的总数量
     */
    public static Map<Long, Long> countSubQas(List<Long> subCateIds) {
        Map<Long, Long> result = Maps.newHashMap();
        for (Long subCateId : subCateIds) {
            long c = QuestionAnswer.count("isDelete = 0 and subCateId=" + subCateId);
            result.put(subCateId, c);
        }
        return result;
    }


    /**
     * 查询章节下，各个子章节的总数量
     */
    public static Map<Long, Long> countChildQas(List<Long> childCateIds) {
        Map<Long, Long> result = Maps.newHashMap();
        for (Long childCateId : childCateIds) {
            long c = QuestionAnswer.count("isDelete = 0 and childCateId=" + childCateId);
            result.put(childCateId, c);
        }
        return result;
    }
}
