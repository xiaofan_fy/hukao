package models.services;

import com.tom.basic.service.BaseQuery;
import com.tom.basic.service.ServiceQueryResult;
import models.KeJian;
import support.ModelPaginatorUtils;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class KeJianService {

    public static ServiceQueryResult<KeJian> queryData(Long bigCateId, Long cateId, int page, int limit) {
        ServiceQueryResult<KeJian> result = new ServiceQueryResult<>(true, null);
        BaseQuery queryParams = new BaseQuery();
        queryParams.setPageIndex(page);
        queryParams.setPageSize(limit);
        List<Object> params = new ArrayList<Object>();
        StringBuilder querySql = new StringBuilder("From KeJian  where isDelete = 0 ");
        if (bigCateId != null) {
            querySql.append(" and  selectBigCateId = ? ");
            params.add(bigCateId);
        }
        if (cateId != null) {
            querySql.append(" and  subCateId = ? ");
            params.add(cateId);
        }
        Query query = KeJian.em().createQuery(querySql + " ORDER BY sortOrder DESC");
        ModelPaginatorUtils.setParams2Query(params, query, queryParams);
        result.obj = query.getResultList();
        query = KeJian.em().createQuery("select count(*) " + querySql);
        ModelPaginatorUtils.setParams2Query(params, query);
        result.totalCount = (Long) query.getSingleResult();
        return result;
    }

}
