package models.services;

import models.UserHistory;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class UserHistoryService {

    /**
     * 获取用户已经回答过正确的试题
     */
    public static Map<Long, Long> getUserHadTestData(String openId) {
        List<UserHistory> userHistoryList = UserHistory.find("isDelete = 0 and openId=:openId and isCorrect = 1").setParameter("openId",
                openId).fetch();
        return userHistoryList.stream().collect(Collectors.groupingBy(e -> e.subCateId, Collectors.counting()));
    }


    /**
     * 获取用户已经回答过正确的试题
     */
    public static Map<Long, Long> getUserHadTestSubData(Long subCateId, String openId) {
        List<UserHistory> userHistoryList =
                UserHistory.find("isDelete = 0 and openId=:openId and isCorrect = 1 and subCateId=:subCateId")
                        .setParameter("openId", openId)
                        .setParameter("subCateId", subCateId).fetch();
        return userHistoryList.stream().collect(Collectors.groupingBy(e -> e.childCateId, Collectors.counting()));
    }
}
