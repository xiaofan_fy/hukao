package models.services;

import com.tom.basic.service.BaseQuery;
import com.tom.basic.service.ServiceQueryResult;
import models.QuestionAnswer;
import support.ModelPaginatorUtils;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class QuestionAnswerService {

    public static ServiceQueryResult<QuestionAnswer> queryData(Long id, Long bigCateId, Long subCateId, Long childCateId, int page,
                                                               int limit) {
        ServiceQueryResult<QuestionAnswer> result = new ServiceQueryResult<>(true, null);
        BaseQuery queryParams = new BaseQuery();
        queryParams.setPageIndex(page);
        queryParams.setPageSize(limit);
        List<Object> params = new ArrayList<Object>();
        StringBuilder querySql = new StringBuilder("From QuestionAnswer  where isDelete = 0");
        if (bigCateId != null) {
            querySql.append(" and  bigCateId = ? ");
            params.add(bigCateId);
        }
        if (id != null) {
            querySql.append(" and  id = ? ");
            params.add(id);
        }
        if (subCateId != null) {
            querySql.append(" and  subCateId = ? ");
            params.add(subCateId);
        }
        if (childCateId != null) {
            querySql.append(" and  childCateId = ?");
            params.add(childCateId);
        }
        Query query = QuestionAnswer.em().createQuery(querySql + " ORDER BY sortOrder DESC");
        ModelPaginatorUtils.setParams2Query(params, query, queryParams);
        result.obj = query.getResultList();
        query = QuestionAnswer.em().createQuery("select count(*) " + querySql);
        ModelPaginatorUtils.setParams2Query(params, query);
        result.totalCount = (Long) query.getSingleResult();
        return result;
    }

}
