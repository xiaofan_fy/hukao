package models.services;

import com.tom.basic.service.BaseQuery;
import com.tom.basic.service.ServiceQueryResult;
import models.WeiXinUser;
import support.ModelPaginatorUtils;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class WeiXinUserService {

    public static ServiceQueryResult<WeiXinUser> queryUsers(int page, int limit) {
        ServiceQueryResult<WeiXinUser> result = new ServiceQueryResult<>(true, null);
        BaseQuery queryParams = new BaseQuery();
        queryParams.setPageIndex(page);
        queryParams.setPageSize(limit);
        List<Object> params = new ArrayList<Object>();
        StringBuilder querySql = new StringBuilder("From WeiXinUser  where isDelete = 0");
        Query query = WeiXinUser.em().createQuery(querySql + " ORDER BY id DESC");
        ModelPaginatorUtils.setParams2Query(params, query, queryParams);
        result.obj = query.getResultList();
        query = WeiXinUser.em().createQuery("select count(*) " + querySql);
        ModelPaginatorUtils.setParams2Query(params, query);
        result.totalCount = (Long) query.getSingleResult();
        return result;
    }
}
