package models;

import com.tom.basic.serialize.json.GsonField;
import models.base.BaseModel;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.math.BigDecimal;

@Entity
public class KeJianCategory extends BaseModel {

    public Long bigCateId;

    public String name;

    public String banner;

    public Integer sortOrder;

    public Integer vipSub;

    public BigDecimal fee;

    @Transient
    public String selectBigCateShow;

    @GsonField(name = "vipSubShow")
    public String vipSubShow() {
        if (vipSub == null) {
            return "免费";
        }
        if (0 == vipSub) {
            return "免费";
        }
        if (1 == vipSub) {
            return "分享后免费";
        }
        if (2 == vipSub) {
            return "支付后可看, 费用:" + fee + "￥";
        }
        return "";
    }
}
