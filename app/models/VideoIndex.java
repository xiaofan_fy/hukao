package models;

import com.google.common.collect.Lists;
import com.tom.basic.serialize.json.GsonField;
import models.base.BaseModel;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.util.List;

@Entity
public class VideoIndex extends BaseModel {

    public Long bigCateId;

    public String name;

    public String urls;

    public Integer sortOrder;

    @Transient
    public String cateName;

    public String banner;

    @GsonField(name = "urlsShow")
    public List<String> urlsShow() {
        return Lists.newArrayList(urls.split(","));
    }
}
