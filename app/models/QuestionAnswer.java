package models;

import models.base.BaseModel;

import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
public class QuestionAnswer extends BaseModel {

    public Long bigCateId;

    public Long subCateId;

    public Long childCateId;

    public String question;

    public String selectAbleData;

    public String answerDesc; //答案解析

    public Integer sortOrder;

    public String videoUrl;


    @Transient
    public String bigCateShow;

    @Transient
    public String subCateShow;

    @Transient
    public String childCateShow;

    @Transient
    public String vipSubShow;


    @Transient
    public boolean isUserCollect;

}
