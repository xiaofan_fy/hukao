package models;

import models.base.BaseModel;

import javax.persistence.Entity;

@Entity
public class UserHistory extends BaseModel {

    public Long bigCateId;

    public Long subCateId;

    public Long childCateId;

    public Long qaId;

    public String openId;

    public String dayTestDate;

    public boolean isCorrect; // 客户的答案是否正确
}
