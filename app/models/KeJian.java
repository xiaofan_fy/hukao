package models;

import com.tom.basic.serialize.json.GsonField;
import models.base.BaseModel;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.math.BigDecimal;

@Entity
public class KeJian extends BaseModel {

    public String fileUrl;

    public String title;

    public Integer sortOrder;

    public Integer vipSub; // vip课程

    public Long selectBigCateId;

    @Transient
    public String selectBigCateShow;

    public BigDecimal fee;

    public String banner;

    public String detailUrl;

    public Long subCateId;

    @Transient
    public String subCateShow;

    @GsonField(name = "vipSubShow")
    public String vipSubShow() {
        if (vipSub == null) {
            return "免费";
        }
        if (0 == vipSub) {
            return "免费";
        }
        if (1 == vipSub) {
            return "分享后免费";
        }
        if (2 == vipSub) {
            return "支付后可看, 费用:" + fee + "￥";
        }
        return "";
    }

}
