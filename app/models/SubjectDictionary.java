package models;

import com.tom.basic.serialize.json.GsonField;
import models.base.BaseModel;

import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
public class SubjectDictionary extends BaseModel {

    // 题目的大标题
    public Long bigCateId; // 大类的ID

    public String dictName; // 名称

    public int sortOrder;// 排序

    @Transient
    public Long qaCount;

    @Transient
    public Long percent;

    public String shareImg;

    @GsonField(name = "vipSubShow")
    public String vipSubShow() {
        return "";
    }

}
