package models;

import models.base.BaseModel;

import javax.persistence.Entity;

@Entity
public class WeiXinUser extends BaseModel {

    public WeiXinUser() {
        super();
    }

    public String openId;

    public String avatarUrl;

    public String city;

    public int gender;

    public String nickName;

    public String province;

    public String phoneNumber;
}
