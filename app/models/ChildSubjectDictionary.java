package models;

import com.tom.basic.serialize.json.GsonField;
import models.base.BaseModel;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.math.BigDecimal;

@Entity
public class ChildSubjectDictionary extends BaseModel {

    public String dictName; // 名称

    public int sortOrder;// 排序

    public Long parentId;

    @Transient
    public Long qaCount;


    @Transient
    public Long percent;


    public Integer vipSub; // vip课程

    public BigDecimal fee; // vip课程的价格


    @GsonField(name = "vipSubShow")
    public String vipSubShow() {
        if (vipSub == null) {
            return "免费";
        }
        if (0 == vipSub) {
            return "免费";
        }
        if (1 == vipSub) {
            return "分享后免费";
        }
        if (2 == vipSub) {
            return "支付后可看, 费用:" + fee + "￥";
        }
        return "";
    }
}
